# Meteor.js & Materialize Boilerplate ![Build Passing](https://img.shields.io/badge/build-passing-brightgreen.svg?flat-square) [![MIT licensed](https://img.shields.io/badge/license-MIT-blue.svg)](https://bitbucket.org/Antmounds/meteorjs-materialize-boilerplate/master/LICENSE)

[**Meteor Materialize Boilerplate**](https://bitbucket.org/Antmounds/meteorjs-materialize-boilerplate) is a boilerplate and tooling designed to get you up and running as fast as possible when creating [Meteor.js](https://www.meteor.com/) based web and mobile app. This project uses [MaterializeCSS](http://materializecss.com/media-css.html) for styling and the default [Blaze](https:blazejs.org) templating system for layouts.

**Follow us** on [YouTube](https://www.youtube.com/channel/UCAOCtEujWpYJAbZYJH8vdcA?sub_confirmation=1), [Twitter](https://twitter.com/Antmounds), or [Facebook](https://www.facebook.com/Antmounds) | **Learn** [Meteor.js and ES6](#learn-meteorjs-and-es6) | **Visit our sponsors** :


### Features
- Materialize CSS v1.0
- removed insecure & autopublish
- added aldeed/collections2
- imports based app structure
- SEO optimized HTML HEAD section
- sample android and web builds

### Directory Layout
```bash
ProjectFolder
├── client/
│   ├── main.css						# custom css definitions							
│   ├── main.html						# app HTML head section
│   └── main.js 						# <== Application entry point
│   └── main.js 						# <== application entry point
├── imports/
│   ├── api/							# database and model definitions
│   ├── startup/
│   │   ├── client/
│   │   │   ├── index.js
│   │   │   └── routes.js
│   │   ├── server/
│   │   │   ├── index.js
│   │   │   └── register-api.js
│   │   └── accounts-config.js
│   └── ui/
│       ├── components/					# re-usable app components
│       ├── layouts/					# layouts to be inserted into pages
│       └── pages/						# base layers for app
├── public/
│   └── img/							# Static files such as favicon.ico etc.
├── server/
│   └── main.js
└── package.json
```

### Prerequisites

* [Meteor.js](https://www.meteor.com/) v1.6 or higher

### Getting Started

Just clone the repo and start hacking:

```bash
$ git clone https://bitbucket.org/Antmounds/meteorjs-materialize-boilerplate.git MyApp
$ cd MyApp
$ npm install													# Install project dependencies listed in package.json
$ meteor --settings tools/settings-development.json 			# Compiles the app and opens it in a browser with "live reload"
```

The app should become available at [http://localhost:3000/](http://localhost:3000/).

### How to Contribute

Anyone and everyone is welcome to [contribute](CONTRIBUTING.md) to this project. No single ant built the Antmounds. There are a number of ways to help out:

* [Bug reports](.github/CONTRIBUTING.md#bugs)
* [Pull requests](.github/CONTRIBUTING.md#pull-requests)
* [Feature requests](.github/CONTRIBUTING.md#features)
* [Design & Graphics](.github/CONTRIBUTING.md#design)
* [Marketing](.github/CONTRIBUTING.md#marketing)
* [Documentation](.github/CONTRIBUTING.md#documentation)

If this project saved you some valuable time consider buying us a coffee with a [PayPal](https://paypal.me/Antmounds) donation, or pledge monthly on our [Patreon](https://www.patreon.com/Antmounds) page and get access to a private Discord support channel. We also accept [Bitcoin](https://blockchain.info/address/), [Ether]() & [Litecoin](). Thoughts & prayers are appreciated too!

### License
Copyright © 2017-present Antmounds. This source code is licensed under the MIT license found in
the [LICENSE](https://bitbucket.org/Antmounds/meteorjs-materialize-boilerplate/LICENSE) file.